/*compress_main.cpp
MIT License

Copyright (c) 2017 Yoshimasa Takabatake

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <string>
#include <chrono>

#include "solca.hpp"
#include "decomp_suc_poslp.hpp"
#include "cmdline.h"
#include "slp_to_repair.hpp"

#include "BitVec.hpp"
#include "WBitsVec.hpp"

using namespace std;

int main(int argc, char *argv[])
{
  cmdline::parser p;
  p.add<string>("input_file",  'i', "input file name",  true);
  p.add<string>("output_file", 'o', "output file name", true);
  p.add<string>("turning_point", 't', "turning point of RePair (default=3)", false);
  p.add<bool>  ("erase_br",    'e', "erase line break", false);

  p.parse_check(argc, argv);
  const string input_file   = p.get<string>("input_file");
  const string output_file  = p.get<string>("output_file");
  const bool   erase_br     = p.get<bool>  ("erase_br");

  uint32_t turn_point = 3;
  if( !p.get<string>("turning_point").empty() )
    turn_point = stoi(p.get<string>("turning_point"));

  auto t1 = std::chrono::high_resolution_clock::now();
  auto start = t1;
  {
    solca_comp::SOLCA solca;
    if(solca.Compress(input_file, output_file, erase_br) != 1){
      std::cout << "failed at solca?" << std::endl;
      return 0;
    }
  }

  uint64_t num_rules = 0;
  itmmti::BitVec<> skelton;
  itmmti::WBitsVec leaf;
  /*
  {
    solca_comp::DSucPOSLP dsuc_poslp;
    auto dsolca = dsuc_poslp.DecompressStructure(output_file);
  }
*/

  {
     solca_comp::SOLCA solca;
     if (solca.Compress(input_file,output_file,erase_br) != 1) {
       std::cout << "failed at solca?" << std::endl;
       return 0;
     }

     num_rules = solca.GetNumRules() - 256;
     skelton.resize(2 * num_rules + 1);
     leaf.convert(itmmti::bits::bitSize(num_rules + 256), num_rules + 1);
     leaf.resize(num_rules + 1);
     // copy solca grammar
     uint32_t inner_i = 256;
     uint32_t leaf_i = 0;
     const uint32_t length = solca.Length() - 1; // -1 to remove bit for super root
     for (uint32_t i = 0; i < length; ++i) {
       const uint8_t bit = solca.GetBit(i);
       skelton.writeBit(bit, i);
       if (bit) { // leaf
         leaf[leaf_i] = solca.GetLeaf(leaf_i, inner_i);
         ++leaf_i;
       } else { // internal node
         ++inner_i;
       }
     }
     solca.Delete();
   } // solca is dropped


  {//debug
  // leaf.printStatistics(true);
  //   for (uint32_t i = 0; i < leaf.size(); ++i) {
  //     std::cout << i << ":" << leaf[i] << ", ";
  //   }
  //   std::cout << std::endl;
  }

  {
    auto t2 = std::chrono::high_resolution_clock::now();
    double sec = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    std::cout << "SOLCA done in " << sec/1000.0 << " sec" << std::endl << std::endl;
    t1 = t2;
  }

  // slp to repair starts
  SLPToRePair(input_file, output_file, turn_point, num_rules, std::move(skelton), std::move(leaf));
  {
    auto t2 = std::chrono::high_resolution_clock::now();
    double sec = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
    std::cout << "SLP to RePair done in " << sec/1000.0 << " sec" << std::endl;
    t1 = t2;
  }
  auto end = std::chrono::high_resolution_clock::now();
  double all_time = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
  std::cout << all_time/1000.0 << " sec" << std::endl;

  return 1;
}

