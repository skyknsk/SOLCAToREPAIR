#include "slp_to_repair.hpp"


void SLPToRePair(
    std::string input_file,
    std::string output_file,
    uint32_t turn_point,
    uint64_t num_rules, 
    itmmti::BitVec64 skelton, 
    itmmti::WBitsVec leaf
    ){
      uint32_t size_seq, repair_var_id;
      SEQ *naive_seq;
      RULE *dict_rule;
      {
        slp_repair::RePair repair(num_rules, std::move(skelton), std::move(leaf));
        repair.RePairRecompression(input_file, turn_point);
        size_seq = repair.GetSeqSize();
        repair_var_id = repair.GetRepairVarId() - 257;
        naive_seq = std::move(repair.GetNaiveSeq());
        dict_rule = std::move(repair.GetDictRule());
      }
      LinearTimeRePair(repair_var_id, size_seq, output_file, std::move(naive_seq), std::move(dict_rule));
}


void LinearTimeRePair
(
  uint32_t num_rules,
  uint32_t size_seq,
  std::string output_file,
  SEQ *naive_seq,
  RULE *dict_rule
){
    auto start = std::chrono::system_clock::now();
    std::cout << "RePair 2nd step..." << std::endl;

    FILE *output;
    DICT *dict;
    EDICT *edict;

    // output_file += ".rp";
    output = fopen(output_file.c_str() , "wb");

    dict = RunRepair(std::move(naive_seq), std::move(dict_rule), size_seq, num_rules);
    auto repair_end = std::chrono::system_clock::now();
    auto repair_sec = std::chrono::duration_cast<std::chrono::milliseconds>(repair_end-start).count();
    std::cout << "Compute RePair Grammar in " << repair_sec/1000.0 << " sec" << std::endl;

    edict = convertDict(dict);
    EncodeCFG(edict, output);
    DestructEDict(edict);

    auto end = std::chrono::system_clock::now();       
    auto dur = end - start;        
    auto sec = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
    std::cout << "SLP to RePair 2nd step done in " << sec/1000.0 << " sec" << std::endl << std::endl;
    fclose(output);
}
