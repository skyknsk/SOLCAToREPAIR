#include "RePair.hpp"
#include "linear_time_repair.hpp"
#include "encoder.hpp"

#include "BitVec.hpp"
#include "WBitsVec.hpp"

void SLPToRePair(
    std::string input_file,
    std::string output_file,
    uint32_t turn_point,
    uint64_t num_rules, 
    itmmti::BitVec64 skelton, 
    itmmti::WBitsVec leaf
);

void LinearTimeRePair(
    uint32_t num_rules, 
    uint32_t size_seq,
    std::string output_file,
    SEQ *naive_seq,
    RULE *dict_rule
);
