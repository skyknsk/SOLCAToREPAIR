# solca to repair

An implementation of the RePair algorithm proposed in [RePair in Compressed Space and Time](https://arxiv.org/abs/1811.01472) [3].  
This program first compresses a text using SOLCA and then to recompress it to get the RePair grammar of the text.  
The implementation of SOLCA is from [https://github.com/tkbtkysms/solca](https://github.com/tkbtkysms/solca).  

# download and compile
    $ git clone git@gitlab.com:skyknsk/SOLCAToREPAIR.git  
    $ cd SOLCAToREPAIR/src/  
    $ make

# executions
## compression
    $ ./compress --input_file=string --output_file=string [options] ...   
     options:  
       -i, --input_file     input file name (string)  
       -o, --output_file    output file name (string)  
       -t, --turnig_point   turning point of RePair (default=3)  
       -e, --erase_br       erase line break (bool [=0])  
       -?, --help           print this message  

## decompression
    $ ./decompress --compressed_file=string --output_file=string [options] ...   
     options:  
       -i, --compressed_file    compressed file name (string)  
       -o, --output_file        output file name (string)  
       -?, --help               print this message  
  
# experiments for [repetitive texts of Pizza & chili corpus](http://pizzachili.dcc.uchile.cl/repcorpus/real/)
## comparison methods
FOLCA: [1].  
FOLCA+: FOLCA applying SOLCA's dynamic succinct tree.  
SOLCA: Seciton 3.3 of [2].  
SOLCA+CRD: Section 4 of [2]   
LZD: the patriciaf tree　space construction of https://github.com/kg86/lzd.  
RP: https://code.google.com/archive/p/re-pair/.  
SERP: https://github.com/nicolaprezza/Re-Pair.  
SOLCAToRP: [3].  

## experimental environment
OS: CentOS 6.10  
CPU: Intel(R) Xeon(R) CPU E7-8837 @2.67GHz  
Memory: 1TB  
Compiler: gcc 7.3.1 

## eavaluation measure
CT: compression time  
WS: working space  
CR: compression (compressed size/ input size * 100)

## cere (440MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|428.65|168.24|3.82|
|FOLCA+|303.88|168.19|3.82|
|SOLCA|532.23|60.45|3.82| 
|SOLCA+CRD|183.66|82.45|3.82|
|LZD|26.94|589.76|5.92|
|RP|157.54|5409.72|1.97|
|SERP|4352.02|2834.93|1.93|
|SOLCAToRP(t=2)|347.41|2896.98|1.96|
|SOLCAToRP(t=3)|533.16|2001.40|1.96|
|SOLCAToRP(t=4)|1387.49|1546.85|1.95|
|SOLCAToRP(t=5)|6009.66|1289.74|1.95|


<!-- ##  core utils (196MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|213.56|140.96|6.04|
|FOLCA+|152.05|140.92|6.04|
|SOLCA|301.90|39.08|6.04|
|SOLCA+CRD|82.98|61.09|6.04|
|LZD|11.95|334.04|8.96|
|Re-Pair|1798.69|1175.77|2.77|

## einstein.de.txt (89MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|84.33|4.68|0.38|
|FOLCA+|59.51|4.68|0.38|
|SOLCA|103.61|1.36|0.38|
|SOLCA+CRD|14.77|23.36|0.38|
|LZD|2.07|46.28|0.66|
|Re-Pair|234.21|464.48|0.19| -->

## einstein.en.txt (446MB) 

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|428.11|10.60|0.20|
|FOLCA+|305.27|10.60|0.20|
|SOLCA|548.43|3.29|0.20|
|SOLCA+CRD|78.63|25.29|0.20|
|LZD|9.79|205.06|0.28|
|RP|180.65|5358.61|0.08|
|SERP|1450.78|2417.07|0.10|
|SOLCAToRP(t=2)|209.12|2692.82|0.08|
|SOLCAToRP(t=3)|273.10|1801.04|0.08|
|SOLCAToRP(t=4)|461.62|1355.99|0.08|
|SOLCAToRP(t=5)|698.65|1087.96|0.08|

## Escherichia_coli (108MB) 

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|114.68|159.63|14.14|
|FOLCA+|81.31|159.58|14.14|
|SOLCA|173.70|56.58|14.14|
|SOLCA+CRD|67.42|78.58|14.14|
|LZD|14.72|403.53|20.98|
|RP|45.66|1390.87|8.98|
|SERP|1188.51|701.24|8.37|
|SOLCAToRP(t=2)|161.92|868.87|8.87|
|SOLCAToRP(t=3)|397.10|627.51|8.86|
|SOLCAToRP(t=4)|1340.25|496.87|8.87|
|SOLCAToRP(t=5)|6685.70|446.02|8.87|

## influenza (148MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|140.62|83.66|5.44|
|FOLCA+|97.57|83.64|5.44|
|SOLCA|196.95|22.92|5.44|
|SOLCA+CRD|41.84|44.92|5.44|
|LZD|8.39|215.49|6.47|
|RP|57.06|1808.09|2.63|
|SERP|574.54|960.53|2.67|
|SOLCAToRP(t=2)|104.86|1016.48|2.55|
|SOLCAToRP(t=3)|234.67|731.89|2.55|
|SOLCAToRP(t=4)|907.19|581.27|2.55|
|SOLCAToRP(t=5)|2406.11|488.88|2.55|

## kernel (247MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|265.48|73.79|2.55|
|FOLCA+|187.72|73.76|2.55|
|SOLCA|336.59|21.32|2.55|
|SOLCA+CRD|102.55|43.32|2.55|
|LZD|9.46|257.53|4.20|
|RP|106.42|3002.86|1.15|
|SERP|1969.48|1374.04|1.23|
|SOLCAToRP(t=2)|607.90|1562.24|1.15|
|SOLCAToRP(t=3)|1872.99|1062.23|1.15|
|SOLCAToRP(t=4)|4485.57|818.50|1.15|
|SOLCAToRP(t=5)|8498.85|665.24|1.15|

## para (410MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|419.36|192.01|5.21|
|FOLCA+|289.32|191.97|5.21|
|SOLCA|522.65|72.46|5.21|
|SOLCA+CRD|196.63|94.46|5.21|
|LZD|30.92|693.93|8.19|
|RP|154.95|5070.77|2.82|
|SERP|5011.45|2639.32|2.74|
|SOLCAToRP(t=2)|464.41|2757.60|2.82|
|SOLCAToRP(t=3)|800.23|1931.72|2.82|
|SOLCAToRP(t=4)|2222.27|1492.50|2.82|
|SOLCAToRP(t=5)|9496.45|1264.19|2.82|

## world_leaders (45MB)

|compressor|CT (sec)|WS (MB)|CR (%)|
|---|---|---|---|
|FOLCA|41.16|21.24|4.26|
|FOLCA+|29.69|21.23|4.26|
|SOLCA|60.15|6.54|4.26|
|SOLCA+CRD|10.46|28.54|4.26|
|LZD|1.60|53.52|5.41|
|RP|12.65|549.09|1.54|
|SERP|103.45|242.30|1.71|
|SOLCAToRP(t=2)|21.11|296.37|1.54|
|SOLCAToRP(t=3)|41.58|210.91|1.54|
|SOLCAToRP(t=4)|109.55|166.73|1.54|
|SOLCAToRP(t=5)|200.57|138.95|1.54|


# references
[1] Shirou Maruyama, Yasuo Tabei, Hiroshi Sakamoto, Kunihiko Sadakane:  
Fully-Online Grammar Compression. SPIRE 2013, 218-229.  

[2] Yoshimasa Takabatake, Tomohiro I, Hiroshi Sakamoto:  
A Space-Optimal Grammar Compression. ESA 2017: 67:1-67:15  

[3] Kensuke Sakai, Tatsuya Ohno, Keisuke Goto, Yoshimasa Takabatake, Tomohiro I, Hiroshi Sakamoto:  
RePair in Compressed Space and Time.

